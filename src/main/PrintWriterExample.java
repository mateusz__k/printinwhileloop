package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class PrintWriterExample {

    public static void main(String[] args) {


        //  "REGUŁKA" DANYCH DO PLIKU


        File f = new File("NewFile");
        System.out.println("Czy plik istnieje: " + f.exists());

        try {
            FileOutputStream fos = new FileOutputStream(f);        // TWORZE STREAM
            PrintWriter writer = new PrintWriter(fos);             // TWORZE WRITER

            writer.println("jakaś treść");                          // UZYWAM WRITERA DO ZAPISU W FOLDERZE

            writer.close();                                         // ZAWSZE ZAMYKAM WRITERA PO UZYCIU
        } catch (FileNotFoundException e) {
            e.printStackTrace();                                    // CATCH EXCEPTION
        }


        //KONIEC "REGUŁKI


        //REGUŁKA Z AUTOMATYCZNYM ZAMYKANIEM



    }
    }

