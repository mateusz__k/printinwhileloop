package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class FirstExercise {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        File records = new File("records");

        try {
            while (!input.equals("quit")) {
                FileOutputStream stream = new FileOutputStream(records, true);
                PrintWriter writer = new PrintWriter(stream);

                writer.println(input);
                input = scanner.next();


                writer.close();

            }
        }  catch(FileNotFoundException file){
                file.printStackTrace();

            }
        }

    }
